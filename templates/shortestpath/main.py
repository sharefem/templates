"""
Solves the brachistochrone problem with fenics
"""
import dolfin as fn
import matplotlib.pyplot as plt


nx = 50
mesh = fn.UnitIntervalMesh(nx)
Aref = fn.Constant(1.0)
Bref = fn.Constant(0.0)
order = 1

V = fn.FunctionSpace(mesh, 'CG', order)

u = fn.interpolate(fn.Expression('x[0]*x[0]-2.0*x[0]+1.0',
                   degree=order+1), V)
plt.subplot(211)
fn.plot(u, title='u0')

vstart = fn.Constant(0.0001)
# Shortest path
G = fn.sqrt(1.0+fn.Dx(u, 0)**2)
Ef = G*fn.dx

left = fn.CompiledSubDomain("near(x[0], side) && on_boundary", side=0.0)
right = fn.CompiledSubDomain("near(x[0], side) && on_boundary", side=1.0)
bcl = fn.DirichletBC(V, Aref, left)
bcr = fn.DirichletBC(V, Bref, right)
bcs = [bcl, bcr]

dEf = fn.derivative(Ef, u)
d2Ef = fn.derivative(dEf, u)

problem = fn.NonlinearVariationalProblem(dEf, u, bcs, d2Ef)
solver = fn.NonlinearVariationalSolver(problem)
prm = solver.parameters

prm['newton_solver']['maximum_iterations'] = 50
prm['newton_solver']['relaxation_parameter'] = 1.0
prm['newton_solver']['absolute_tolerance'] = 1.0e-14
prm['newton_solver']['relative_tolerance'] = 1.0e-8

solver.solve()
print('Shortest path ', fn.assemble(G*fn.dx))

up = fn.project(u, V)

plt.subplot(212)
fn.plot(up, title='u')
plt.savefig("test.png")
